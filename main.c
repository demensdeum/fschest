#include "fschest.h"

int main(int argc, char* argv[]) {
    
    printf("FSChest containers creator/extractor by demensdeum@gmail.com (2020)\n");
    
    if (argc < 2) {
        printf("Make: fschest testDirectory\n");
        printf("Extract: fschest files.chest extractPath\n");
    }
    
    if (argc == 2) {
        FSCHEST_packDirectory(argv[1]);
    }
    else if (argc == 3) {
        FSCHEST_extractChestToDirectory(argv[1], argv[2]);
    }
    
    return 0;
}
