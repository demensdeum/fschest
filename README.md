# Flame Steel Chest AKA FSChest

Flame Steel Chest - dead simple zero compression container format  
  
Format:  

filename  
size in bytes  
binary data  
filename  
size in bytes  
binary data  
  
Usage:  

Make chest: fschest testDirectory  
Extract chest: fschest files.chest extractPath
